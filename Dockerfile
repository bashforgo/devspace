# Download docker
FROM docker:19.03.8 as download-docker

# Download kubectl
FROM alpine:3.11 as download-kubectl
ENV KUBECTL_VERSION v1.17.3
ENV KUBECTL_URL https://dl.k8s.io/${KUBECTL_VERSION}/kubernetes-client-linux-amd64.tar.gz
RUN wget -O kubectl.tar.gz "${KUBECTL_URL}"
RUN tar -xvf kubectl.tar.gz --strip-components 3

# Download skaffold
FROM alpine:3.11 as download-devspace
ENV DEVSPACE_VERSION v4.11.1
ENV DEVSPACE_URL https://github.com/devspace-cloud/devspace/releases/download/${DEVSPACE_VERSION}/devspace-linux-amd64
RUN wget -O devspace "${DEVSPACE_URL}"
RUN chmod +x devspace

FROM alpine:3.11
COPY --from=download-docker /usr/local/bin/docker /usr/local/bin/
COPY --from=download-kubectl kubectl /usr/local/bin/
COPY --from=download-devspace devspace /usr/local/bin/

LABEL version="v4.11.1"
